package br.com.mcampos.poc1

import android.annotation.SuppressLint
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
private const val REQUEST_CHECK_SETTINGS: Int = 1


class MainActivity : AppCompatActivity() {

    private lateinit var mFusedLocation: FusedLocationProviderClient
    private var hasPermission = false
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest


    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mFusedLocation = LocationServices.getFusedLocationProviderClient(this)

        locationRequest = LocationRequest().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        addListenerLastLocation()
        createLocationRequest()
        initLocationCallback()


    }

    fun initLocationCallback() {
        Thread {
            locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return
                    for (location in locationResult.locations) {
                        val lat = location?.latitude
                        val lon = location?.longitude

                        textPosition.text = if (location?.latitude != 0.0) "Latitude $lat longitude $lon e altitude ${location?.altitude}" else ""
                    }
                }

            }

        }.start()

    }

    fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            hasPermission = true;
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
            hasPermission = false;
            when (requestCode) {
                 PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.isNotEmpty()
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        hasPermission = true;
                    }
                }
        }

    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    private fun stopLocationUpdates() {
        mFusedLocation.removeLocationUpdates(locationCallback)
    }

    private var requestingLocationUpdates: Boolean = true

    override fun onResume() {
        super.onResume()
        getLocationPermission()

        if (requestingLocationUpdates) startLocationUpdates()
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        mFusedLocation.requestLocationUpdates(locationRequest,
                locationCallback,
                null /* Looper */)
    }

    @SuppressLint("MissingPermission")
    fun addListenerLastLocation() {

        if (hasPermission) {
            mFusedLocation.lastLocation.addOnSuccessListener { location: Location? ->
                val lat = location?.latitude
                val lon = location?.longitude

                textPosition.text = "Latitude $lat longitude $lon"

            }

            mFusedLocation.lastLocation.addOnFailureListener { exception: Exception ->

                exception.printStackTrace()

            }




        }


    }



    @SuppressLint("RestrictedApi")
    fun createLocationRequest() {


        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)


        val client: SettingsClient = LocationServices.getSettingsClient(this)

        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener { response ->
            Log.d("POC", "Sucesso")
            requestingLocationUpdates = true

        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(this@MainActivity,
                            REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }


    }




}
